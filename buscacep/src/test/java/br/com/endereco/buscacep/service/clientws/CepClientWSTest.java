package br.com.endereco.buscacep.service.clientws;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.buscacep.entity.Cep;
import br.com.endereco.buscacep.service.CepService;
import br.com.endereco.buscacep.service.clientws.CepClientWS;

/**
 * Classe criada para testar a classe CepClientWS.
 * <br /> Essa classe depende de acesso externo (http://m.correios.com.br/movel/buscaCepConfirma.do) 
 * <br /> para concluir com sucesso o método buscaCepCorreio.
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CepClientWSTest {

	@Autowired
	private CepClientWS cepClientWS;
	
	/**
	 * Método criado para testar quando enviar um CEP NULL.
	 */
	@Test
	public void testarBuscaCepCorreioCepNull(){
		try{
			cepClientWS.buscaCepCorreio(null);
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar quando for enviado um CEP NULL.
	 */
	@Test
	public void testarBuscaCepCorreioCepVazio(){
		try{
			cepClientWS.buscaCepCorreio("");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar quando for enviado um CEP com letras.
	 */
	@Test
	public void testarBuscaCepCorreioCepLetras(){
		try{
			cepClientWS.buscaCepCorreio("Avenida Rui");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar quando for enviado um CEP inválido.
	 */
	@Test
	public void testarBuscaCepCorreioCepInvalido(){
		try{
			cepClientWS.buscaCepCorreio("12233548");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar o método buscaCepCorreio quando for enviado um cep de cidade inteira.
	 */
	@Test
	public void testarBuscaCepCorreioCepCidadeInteira(){
		String cep = "11740000";
		Cep cepObject = cepClientWS.buscaCepCorreio(cep);
		
		//Validações
		//Número precisa ser igual ao passado
		assertThat(cepObject.getNumeroCep()).isEqualTo(cep);
		//Deve ter cidade
		assertThat(cepObject.getCidade()).isNotNull();
		//Deve ter estado
		assertThat(cepObject.getCidade().getEstado()).isNotNull();
		//Não deve ter bairro
		assertThat(cepObject.getBairro()).isNull();
		//Não deve ter logradouro
		assertThat(cepObject.getLogradouro()).isNull();
		//Não deve ter CPC
		assertThat(cepObject.getCpc()).isNull();
		//Não deve ter UNIDADE
		assertThat(cepObject.getUnidade()).isNull();
	}
	
	/**
	 * Método criado para testar o método buscaCepCorreio quando for enviado um cep de logradouro.
	 */
	@Test
	public void testarBuscaCepCorreioCepLogradouroBairro(){
		String cep = "44570-410";
		Cep cepObject = cepClientWS.buscaCepCorreio(cep);
		
		//Validações
		//Número precisa ser igual ao passado
		assertThat(cepObject.getNumeroCep()).isEqualTo(cep.trim().replace(".", "").replace("-", ""));
		//Deve ter cidade
		assertThat(cepObject.getCidade()).isNotNull();
		//Deve ter estado
		assertThat(cepObject.getCidade().getEstado()).isNotNull();
		//Não deve ter bairro
		assertThat(cepObject.getBairro()).isNotNull();
		//Não deve ter logradouro
		assertThat(cepObject.getLogradouro()).isNotNull();
		//Não deve ter CPC
		assertThat(cepObject.getCpc()).isNull();
		//Não deve ter UNIDADE
		assertThat(cepObject.getUnidade()).isNull();
	}
	
	/**
	 * Método criado para testar o método buscaCepCorreio quando for enviado um cep que contém CPC.
	 */
	@Test
	public void testarBuscaCepCorreioCepComCPC(){
		String cep = "04.855-990 ";
		Cep cepObject = cepClientWS.buscaCepCorreio(cep);
		
		//Validações
		//Número precisa ser igual ao passado
		assertThat(cepObject.getNumeroCep()).isEqualTo(cep.trim().replace(".", "").replace("-", ""));
		//Deve ter cidade
		assertThat(cepObject.getCidade()).isNotNull();
		//Deve ter estado
		assertThat(cepObject.getCidade().getEstado()).isNotNull();
		//Não deve ter bairro
		assertThat(cepObject.getBairro()).isNull();
		//Não deve ter logradouro
		assertThat(cepObject.getLogradouro()).isNotNull();
		//Não deve ter CPC
		assertThat(cepObject.getCpc()).isNotNull();
		//Não deve ter UNIDADE
		assertThat(cepObject.getUnidade()).isNull();
	}
	
	/**
	 * Método criado para testar o método buscaCepCorreio quando for enviado um cep que contém unidade.
	 */
	@Test
	public void testarBuscaCepCorreioCepComUnidade(){
		String cep = "47100970";
		Cep cepObject = cepClientWS.buscaCepCorreio(cep);
		
		//Validações
		//Número precisa ser igual ao passado
		assertThat(cepObject.getNumeroCep()).isEqualTo(cep.trim().replace(".", "").replace("-", ""));
		//Deve ter cidade
		assertThat(cepObject.getCidade()).isNotNull();
		//Deve ter estado
		assertThat(cepObject.getCidade().getEstado()).isNotNull();
		//Não deve ter bairro
		assertThat(cepObject.getBairro()).isNotNull();
		//Não deve ter logradouro
		assertThat(cepObject.getLogradouro()).isNotNull();
		//Não deve ter CPC
		assertThat(cepObject.getCpc()).isNull();
		//Não deve ter UNIDADE
		assertThat(cepObject.getUnidade()).isNotNull();
	}

}
