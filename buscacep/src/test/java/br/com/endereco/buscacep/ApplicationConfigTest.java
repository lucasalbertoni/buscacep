package br.com.endereco.buscacep;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.buscacep.ApplicationConfig;
import br.com.endereco.buscacep.entity.Cep;

/**
 * Classe criada para testar as requisições da aplicação e seus devidos retornos.
 * <br />Essa classe utiliza um serviço externo (url: http://m.correios.com.br/movel/buscaCepConfirma.do),
 * <br /> caso esse servico esteja fora o teste não irá funcinar devedendo ajustar o serviço.
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class ApplicationConfigTest {
	
	/**
	 * Porta da aplicação.
	 */
	@LocalServerPort
	private int port;

	/**
	 * Template utilizado para testes REST.
	 */
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	/**
	 * URL base para testes.
	 */
	private String urlBase = "http://localhost:";
	
	/**
	 * Caminho base do serviço a ser testado.
	 */
	private String serviceBase = "/busca-cep";
	
	/**
	 * Parâmetro base a ser utilizado na requisição do serviço.
	 */
	private String parameter = "?numeroCep=";
	
	/**
	 * Criado para testar caso em que retorna um cep de uma cidade inteira, 
	 * <br />nesse caso não é possível saber logradouro e bairro apenas pelo CEP.
	 * @throws Exception
	 */
	@Test
	public void retonarCepCidadeInteira() throws Exception {
		String cep = "11740000";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
		then(entity.getBody().getBairro()).isNull();
		then(entity.getBody().getLogradouro()).isNull();
		then(entity.getBody().getCidade()).isNotNull();
		then(entity.getBody().getCpc()).isNull();
		then(entity.getBody().getUnidade()).isNull();
	}
	
	/**
	 * Criado para testar caso em que retorna um cep de um logradouro
	 * <br /> nesse caso é possível saber o logradouro e o bairro à partir do CEP.
	 * <br /> Também esta sendo testado a máscara, pois a aplicação deve estar preparada
	 * <br /> para a mesma.
	 * @throws Exception
	 */
	@Test
	public void retonarCepLogradouro() throws Exception {
		String cep = "44570-410";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
		then(entity.getBody().getBairro()).isNotNull();
		then(entity.getBody().getLogradouro()).isNotNull();
		then(entity.getBody().getCidade()).isNotNull();
		then(entity.getBody().getCpc()).isNull();
		then(entity.getBody().getUnidade()).isNull();
	}
	
	/**
	 * Criado para testar caso em que retorna um cep com CPC.
	 * @throws Exception
	 */
	@Test
	public void retonarCepCpc() throws Exception {
		String cep = "04855-990";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
		then(entity.getBody().getBairro()).isNull();
		then(entity.getBody().getLogradouro()).isNotNull();
		then(entity.getBody().getCidade()).isNotNull();
		then(entity.getBody().getCpc()).isNotNull();
		then(entity.getBody().getUnidade()).isNull();
	}
	
	/**
	 * Criado para testar caso em que retorna um cep com CPC.
	 * @throws Exception
	 */
	@Test
	public void retonarCepUnidade() throws Exception {
		String cep = "47100970";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
		then(entity.getBody().getBairro()).isNotNull();
		then(entity.getBody().getLogradouro()).isNotNull();
		then(entity.getBody().getCidade()).isNotNull();
		then(entity.getBody().getCpc()).isNull();
		then(entity.getBody().getUnidade()).isNotNull();
	}
	
	/**
	 * Método criado para testar o comportamento de quando o cep não é informado
	 */
	@Test
	public void retornarCepNaoInformado(){
		String cep = "";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);
		
		then(entity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Método criado para testar o comportamento de quando o cep passado é inválido.
	 */
	@Test
	public void retornarCepInvalido(){
		String cep = "AAAA";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);
		
		then(entity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Método criado para testar o comportamento de quando o cep passado é maior que 8.
	 */
	@Test
	public void retornarCepMaior(){
		String cep = "123456789";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);
		
		then(entity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Método criado para testar o comportamento de quando o cep passado não consegue ser encontrado.
	 */
	@Test
	public void retornarCepEncontradoSegundaTentativa(){
		String cep = "12233548";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);
		
		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	
	/**
	 * Método criado para testar o comportamento de quando o cep passado não consegue ser encontrado.
	 */
	@Test
	public void retornarCepNaoEncontrado(){
		String cep = "11111111";
		ResponseEntity<Cep> entity = this.testRestTemplate.getForEntity(
				this.urlBase + this.port + serviceBase + parameter + cep, Cep.class);
		
		then(entity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
