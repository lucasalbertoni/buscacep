package br.com.endereco.buscacep.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.endereco.buscacep.entity.Cep;
import br.com.endereco.buscacep.service.CepService;
import br.com.endereco.buscacep.service.clientws.CepClientWS;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

/**
 * Classe criada para testar a classe CepService.
 * @author Lucas
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CepServiceTest {

	/**
	 * A classe que será testada.
	 */
	@Autowired
	private CepService cepService;
	
	/**
	 * Classe necessário para concluir testes, mas não será o foco do teste, por isso é um MOCK.
	 */
	@MockBean
	private CepClientWS cepClientWS;
	
	/**
	 * Método criado para testar o método BuscaCep da classe "spy" CepService.
	 * Esse caso é quando é passado NULL no número do cep.
	 */
	@Test
	public void testarBuscaCepNulo(){
		try{
			cepService.buscaCep(null);
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar o método BuscaCep da classe "spy" CepService.
	 * Esse caso é quando é passado VAZIO no número do cep.
	 */
	@Test
	public void testarBuscaCepVazio(){
		try{
			cepService.buscaCep("");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar o método BuscaCep da classe "spy" CepService.
	 * Esse caso é quando é passado um cep inválido.
	 */
	@Test
	public void testarBuscaCepInvalido(){
		try{
			cepService.buscaCep("AAAA");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar o método BuscaCep da classe "spy" CepService.
	 * Esse caso é quando é passado um cep inválido.
	 */
	@Test
	public void testarBuscaCepInvalidoTamanhoMaiorQue8(){
		try{
			cepService.buscaCep("111.111-111");
		}catch (Exception e) {
			assertThat(e.getMessage()).isEqualTo(CepService.CEP_INVALIDO);
		}
	}
	
	/**
	 * Método criado para testar o método BuscaCep da classe "spy" CepService.
	 */
	@Test
	public void testarBuscaCep(){
		Cep cep = new Cep();
		cep.setNumeroCep("111");
		//Como não é o foco será mocado.
		given(this.cepClientWS.buscaCepCorreio(Mockito.anyString())).willReturn(cep);
		assertThat(cepService.buscaCep("111").getNumeroCep()).isEqualTo(cep.getNumeroCep());
	}
}
