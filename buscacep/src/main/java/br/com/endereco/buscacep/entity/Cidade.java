package br.com.endereco.buscacep.entity;

import java.io.Serializable;

/**
 * Classe que representa a Cidade.
 * @author Lucas
 *
 */
public class Cidade implements Serializable{

	private static final long serialVersionUID = -8481050436676582765L;
	
	private String nome;
	private Estado estado;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
