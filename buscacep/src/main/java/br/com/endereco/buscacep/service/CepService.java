package br.com.endereco.buscacep.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.endereco.buscacep.entity.Cep;
import br.com.endereco.buscacep.service.clientws.CepClientWS;

/**
 * Classe criada para serviços relacionados ao CEP.
 * @author Lucas
 *
 */
@Service
public class CepService {
	
	/**
	 * Mensagem de erro padrão.
	 */
	public static String CEP_INVALIDO = "CEP inválido";
	
	/**
	 * Cliente de webservice para consumir serviços externos relacionados ao CEP.
	 */
	@Autowired
	private CepClientWS cepClientWS;
	
	/**
	 * Método que valida e utiliza o serviço de buscar o CEP.
	 * @param numeroCep
	 * @return
	 */
	public Cep buscaCep(String numeroCep){
		numeroCep = validacoesIniciaisCep(numeroCep);
		Cep cepRetorno = buscaCepPeloServico(numeroCep);
		return cepRetorno;
	}

	/**
	 * Método que faz a consulta do CEP através do cliente de web service.
	 * @param numeroCep
	 * @return
	 */
	private Cep buscaCepPeloServico(String numeroCep) {
		Cep cepRetorno = cepClientWS.buscaCepCorreio(numeroCep);
		if(cepRetorno == null){
			for(int i=7; i>-1; i--){
				StringBuilder sb = new StringBuilder(numeroCep);
				sb.setCharAt(i, '0');
				cepRetorno = cepClientWS.buscaCepCorreio(sb.toString());
				if(cepRetorno != null){
					break;
				}
				numeroCep = sb.toString();
			}
		}
		if(cepRetorno == null){
			throw new RuntimeException(CEP_INVALIDO);
		}
		return cepRetorno;
	}

	/**
	 * Método para validar o CEP.
	 * @param numeroCep
	 * @return
	 */
	private String validacoesIniciaisCep(String numeroCep) {
		if(numeroCep == null || numeroCep.isEmpty()){
			throw new RuntimeException(CEP_INVALIDO);
		}
		numeroCep = numeroCep.trim().replace(".", "").replace("-", "");
		if (numeroCep.length() > 8){
			throw new RuntimeException(CEP_INVALIDO);
		}
		//Validar se existem apenas números.
		try{
			Long.parseLong(numeroCep);
		}catch (Exception e) {
			throw new RuntimeException(CEP_INVALIDO);
		}
		return numeroCep;
	}

}
