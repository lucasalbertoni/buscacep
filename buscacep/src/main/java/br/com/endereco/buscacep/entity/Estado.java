package br.com.endereco.buscacep.entity;

import java.io.Serializable;

/**
 * Classe que representa o Estado.
 * @author Lucas
 *
 */
public class Estado implements Serializable{

	private static final long serialVersionUID = 4105461346703084236L;
	
	private String sigla;
	
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

}
