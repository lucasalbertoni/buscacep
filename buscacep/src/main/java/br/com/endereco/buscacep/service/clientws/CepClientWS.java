package br.com.endereco.buscacep.service.clientws;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import br.com.endereco.buscacep.entity.Cep;
import br.com.endereco.buscacep.entity.Cidade;
import br.com.endereco.buscacep.entity.Estado;
import br.com.endereco.buscacep.service.CepService;

/**
 * Classe criada para consumir o serviço externo de buscar o CEP.
 * <br /> Utilizado o serviço externo (http://m.correios.com.br/movel/buscaCepConfirma.do).
 * @author Lucas
 *
 */
@Service
public class CepClientWS {

	/**
	 * Método criado para buscar o CEP direto nos correios.
	 * <br /> Como ele utiliza o serviço externo (http://m.correios.com.br/movel/buscaCepConfirma.do)
	 * <br /> deve sempre ter o cuidado se o serviço externo esta no ar. 
	 * @param numeroCep
	 * @return
	 */
	public Cep buscaCepCorreio(String numeroCep){
		Document doc = null;
		Cep cep = null;
		try{
			Map<String, String> query = getMapCorreio(numeroCep);
			doc = Jsoup.connect("http://m.correios.com.br/movel/buscaCepConfirma.do")
			            .data(query)
			            .post();
			if(doc != null){
				Elements elementos = doc.select(".respostadestaque");
				if(elementos != null && !elementos.isEmpty()){
					cep = createCepByCorreioObject(elementos);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(CepService.CEP_INVALIDO);
		}
		return cep;
	}

	/**
	 * Método criado para definir qual tipo de CEP retornou,
	 * <br /> e à partir do retorno montar o objeto corretamente.
	 * @param elementos
	 * @return
	 */
	private Cep createCepByCorreioObject(Elements elementos) {
		if(elementos.size() == 4 && elementos.get(2).text().split("/").length == 2){
			return parseCepByCorreioLogradouroBairro(elementos);
		} else if(elementos.size() == 4 && elementos.get(2).text().split("/").length == 1){
			return parseCepByCorreioComCPC(elementos);
		} else if(elementos.size() == 2){
			return parseCepByCorreioCidadeInteira(elementos);
		}else if(elementos.size() == 5){
			return parseCepByCorreioComUnidade(elementos);
		}else{
			throw new RuntimeException(CepService.CEP_INVALIDO);
		}
	}

	/**
	 * Método criado para montar o objeto CEP quando o mesmo é um para cidade inteira.
	 * @param elementos
	 * @return
	 */
	private Cep parseCepByCorreioCidadeInteira(Elements elementos) {
		Cep cep = new Cep();
		cep.setCidade(new Cidade());
		cep.getCidade().setNome(elementos.get(0).text().split("/")[0].trim());
		cep.getCidade().setEstado(new Estado());
		cep.getCidade().getEstado().setSigla(elementos.get(0).text().split("/")[1].trim());
		cep.setNumeroCep(elementos.get(1).text().trim());
		return cep;
	}

	/**
	 * Método criado para montar o CEP quando possui CPC.
	 * @param elementos
	 * @return
	 */
	private Cep parseCepByCorreioComCPC(Elements elementos) {
		Cep cep = new Cep();
		cep.setLogradouro(elementos.get(0).text().trim());
		cep.setCidade(new Cidade());
		cep.getCidade().setNome(elementos.get(1).text().split("/")[0].trim());
		cep.getCidade().setEstado(new Estado());
		cep.getCidade().getEstado().setSigla(elementos.get(1).text().split("/")[1].trim());
		cep.setNumeroCep(elementos.get(2).text().trim());
		cep.setCpc(elementos.get(3).text().trim());
		return cep;
	}

	/**
	 * Método criado para montar um CEP com Logradouro e bairro, mas sem unidade e sem CPC.
	 * @param elementos
	 * @return
	 */
	private Cep parseCepByCorreioLogradouroBairro(Elements elementos) {
		Cep cep = new Cep();
		cep.setLogradouro(elementos.get(0).text().trim());
		cep.setBairro(elementos.get(1).text().trim());
		cep.setCidade(new Cidade());
		cep.getCidade().setNome(elementos.get(2).text().split("/")[0].trim());
		cep.getCidade().setEstado(new Estado());
		cep.getCidade().getEstado().setSigla(elementos.get(2).text().split("/")[1].trim());
		cep.setNumeroCep(elementos.get(3).text().trim());
		return cep;
	}

	/**
	 * Método criado para CEPs que tem retorno Unidade.
	 * @param elementos
	 * @return
	 */
	private Cep parseCepByCorreioComUnidade(Elements elementos) {
		Cep cep = new Cep();
		cep.setLogradouro(elementos.get(0).text().trim());
		cep.setBairro(elementos.get(1).text().trim());
		cep.setCidade(new Cidade());
		cep.getCidade().setNome(elementos.get(2).text().split("/")[0].trim());
		cep.getCidade().setEstado(new Estado());
		cep.getCidade().getEstado().setSigla(elementos.get(2).text().split("/")[1].trim());
		cep.setNumeroCep(elementos.get(3).text().trim());
		cep.setUnidade(elementos.get(4).text().trim());
		return cep;
	}
	
	/**
	 * Método criado para montar o MAP para requisição do correio.
	 * @param numeroCep
	 * @return
	 */
	private Map<String, String> getMapCorreio(String numeroCep){
		Map<String, String> query = new HashMap<String, String>();
        query.put("cepEntrada", numeroCep);
        query.put("tipoCep", "");
        query.put("cepTemp", "");
        query.put("metodo", "buscarCep");
        return query;
	}
}
