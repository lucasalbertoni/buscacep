package br.com.endereco.buscacep.controller.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import br.com.endereco.buscacep.entity.Cep;
import br.com.endereco.buscacep.service.CepService;

/**
 * Classe criada para disponibilizar os serviços REST do CEP.
 * @author Lucas
 *
 */
@RestController
public class CepWS {
	
	/**
	 * Serviço responsável pelo CEP.
	 */
	@Autowired
	private CepService cepService;

	/**
	 * Método criado para buscar o CEP completo à partir do número do cep passado.
	 * @param numeroCep
	 * @return
	 */
	@RequestMapping("/busca-cep")
	public Cep getCep(@RequestParam(value="numeroCep", defaultValue="") String numeroCep){
		try{
			return cepService.buscaCep(numeroCep);
		}catch(Exception e){
			throw new RestClientException(e.getMessage());
		}
	}
}
