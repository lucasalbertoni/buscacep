package br.com.endereco.buscacep.entity;

import java.io.Serializable;

/**
 * Classe que representa o CEP.
 * @author Lucas
 *
 */
public class Cep implements Serializable{

	private static final long serialVersionUID = 1175765446009581836L;
	
	private String numeroCep;
	private String logradouro;
	private String bairro;
	private String cpc;
	private String unidade;
	private Cidade cidade;
	
	public String getNumeroCep() {
		return numeroCep;
	}
	public void setNumeroCep(String numeroCep) {
		this.numeroCep = numeroCep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCpc() {
		return cpc;
	}
	public void setCpc(String cpc) {
		this.cpc = cpc;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
