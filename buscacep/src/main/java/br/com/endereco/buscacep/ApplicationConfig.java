package br.com.endereco.buscacep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe criada para as configurações da aplicação.
 * <br />Utilizando o padrão do Spring Boot.
 * @author Lucas
 *
 */
@SpringBootApplication
public class ApplicationConfig {
	
	/**
	 * Método principal que sobe o servidor.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationConfig.class, args);
	}
}
