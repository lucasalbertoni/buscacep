package br.com.endereco.buscacep.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Classe criada para redirecionar para uma página padrão.
 * <br /> Esse não é o foco, foi feito apenas para não ficar vazio.
 * @author Lucas
 *
 */
@Controller
public class IndexController {
	
	/**
	 * Método para retornar a index.html.
	 * @return
	 */
	@RequestMapping("/")
	public String home(){
		return "index";
	}
	
}
